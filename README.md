#  Exercise_MVC
---
### 후랑노트 MVC 패턴 예제 [🔗](https://furang-note.tistory.com/39)

#

**테스트 Data**

```json
[
    {
        "id": 1,
        "title": "버피테스트",
        "sets": 5,
        "exercise": 40,
        "rest": 20,
        "description": "힘들어도 털고 일어나"
    },
    {
        "id": 2,
        "title": "싸이클",
        "sets": 20,
        "exercise": 120,
        "rest": 120,
        "description": null
    },
    {
        "id": 3,
        "title": "Push-Up",
        "sets": 9,
        "exercise": 20,
        "rest": 10,
        "description": "한 개라도 더"
    },
    {
        "id": 4,
        "title": "Pull-Up",
        "sets": 3,
        "exercise": 20,
        "rest": 10,
        "description": "조져버리자"
    },
    {
        "id": 5,
        "title": "스쾃",
        "sets": 5,
        "exercise": 40,
        "rest": 20,
        "description": null
    },
    {
        "id": 6,
        "title": "Push-Up_Valley",
        "sets": 10,
        "exercise": 20,
        "rest": 10,
        "description": null
    },
    {
        "id": 7,
        "title": "Running_HIIT",
        "sets": 10,
        "exercise": 10,
        "rest": 20,
        "description": null
    },
    {
        "id": 8,
        "title": "풀스쾃",
        "sets": 5,
        "exercise": 40,
        "rest": 20,
        "description": null
    },
    {
        "id": 9,
        "title": "스쾃 +12",
        "sets": 3,
        "exercise": 40,
        "rest": 20,
        "description": null
    },
    {
        "id": 10,
        "title": "스쾃 +16",
        "sets": 3,
        "exercise": 40,
        "rest": 20,
        "description": null
    },
    {
        "id": 11,
        "title": "스쾃 +18",
        "sets": 3,
        "exercise": 40,
        "rest": 20,
        "description": null
    },
    {
        "id": 12,
        "title": "스쾃 +20",
        "sets": 3,
        "exercise": 40,
        "rest": 20,
        "description": null
    }
]
```


