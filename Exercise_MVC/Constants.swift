//
//  Constants.swift
//  Exercise_MVC
//
//  Created by Eun-gwang Moon on 2021/03/18.
//

import Foundation

class Constants {
//    let BASE_URL = "http://192.168.4.121:8080"
    static let BASE_URL = "http://172.30.1.31:8080"
    
    // SUB_URL
    static let ROUTINE_GET_LIST = "/routine/getList"
    static let EXERCISE_GET_LIST = "/exercise/getList"
    static let EXERCISE_GET_ITEM_LIST = "/exercise/getItemList"
    static let EXERCISE_GET_ITEM = "/exercise/getItem"
    static let EXERCISE_INSERT_ITEM = "/exercise/insertItem"
    static let EXERCISE_UPDATE_ITEM = "/exercise/updateItem"
}
