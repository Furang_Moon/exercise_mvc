//
//  ApiService.swift
//  Exercise_MVC
//
//  Created by Eun-gwang Moon on 2021/03/18.
//

import Foundation
import Alamofire

class ApiManager {
    
    static let shared: ApiManager = ApiManager.init()
    
    func selectData<T: Decodable>(with param: Encodable? = nil, from url: String, callback: @escaping (_ data: T?, _ error: String?) -> ()) {
        AF.request(url, method: .post, parameters: param?.dictionary).responseJSON { response in
            guard let resData = response.data else {
                callback(nil, "data is nil")
                return
            }
            do {
                let data = try JSONDecoder().decode(T.self, from:resData)
                callback(data, nil)

            } catch {
                callback(nil, error.localizedDescription)
            }
        }
    }
}
