//
//  Exercise.swift
//  Exercise_MVC
//
//  Created by Eun-gwang Moon on 2021/03/18.
//

import Foundation

struct Exercise: Codable {
    let id: Int
    var title: String
    var sets: Int
    var exercise: Int
    var rest: Int
    var description: String?
}
