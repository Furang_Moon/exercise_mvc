//
//  ExerciseCollectionViewCell.swift
//  Exercise_MVC
//
//  Created by Eun-gwang Moon on 2021/03/18.
//

import UIKit

class ExerciseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    @IBOutlet weak var exerciseLabel: UILabel!
    @IBOutlet weak var restLabel: UILabel!
    var id: Int!
    
    func updateExerciseData(_ exercise: Exercise) {
        self.id = exercise.id
        self.titleLabel.text = exercise.title
        self.setsLabel.text = "\(exercise.sets)"
        self.exerciseLabel.text = "\(exercise.exercise) sec"
        self.restLabel.text = "\(exercise.rest) sec"
    }
}
