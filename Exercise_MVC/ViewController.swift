//
//  ViewController.swift
//  Exercise_MVC
//
//  Created by Eun-gwang Moon on 2021/03/18.
//

import UIKit

class ViewController: UIViewController {
    
    // View를 포함
    @IBOutlet weak var exerciseCollectionView: UICollectionView!
    
    var exerciseList = [Exercise]() {
        didSet {
            // [Exercise]가 Set되면 CollectionView reloadData()
            self.exerciseCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        exerciseCollectionView.dataSource = self
        exerciseCollectionView.collectionViewLayout = self.getCollectionViewLayout()
        getExerciseList()
    }
    
    func getExerciseList() {
        let url = Constants.BASE_URL+Constants.EXERCISE_GET_LIST
        // 데이터 요청
        ApiManager.shared.selectData(from: url) { (data: [Exercise]?, error) in
            guard let data = data else {
                print("error: \(error?.debugDescription)")
                return
            }
            // 데이터 Set
            self.exerciseList = data
        }
    }
}

// MARK:- UICollectionViewDataSource
extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exerciseList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell", for: indexPath) as! ExerciseCollectionViewCell
        cell.contentView.layer.cornerRadius = 5
        let exercise = exerciseList[indexPath.row]
        cell.id = exercise.id
        cell.titleLabel.text = exercise.title
        cell.setsLabel.text = "\(exercise.sets)"
        cell.exerciseLabel.text = "\(exercise.exercise) sec"
        cell.restLabel.text = "\(exercise.rest) sec"
        return cell
    }
    
    func getCollectionViewLayout() -> UICollectionViewLayout {
        let layout:UICollectionViewFlowLayout =  UICollectionViewFlowLayout()
        let cellWidth: CGFloat = UIScreen.main.bounds.width / 2 - 25
        let cellHeight: CGFloat = 130
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return layout as UICollectionViewLayout
    }
}
